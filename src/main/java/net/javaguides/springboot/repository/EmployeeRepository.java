package net.javaguides.springboot.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.Employee;

import java.util.Optional;

@RepositoryRestController
public interface EmployeeRepository extends MongoRepository<Employee, Long> {
   Optional<Employee> findByEmailId(String email);



}
