package net.javaguides.springboot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.mail	.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
import java.util.Date;
import java.util.Random;

@Data
@AllArgsConstructor
//@Table(name = "employees")
@Builder
public class Employee {

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

//	@Column(name = "first_name")
	private String firstName;

//	@Column(name = "last_name")
	private String lastName;

//	@Column(name = "email_id")
	private String emailId;
//	@Column(name = "file")
	private String file;

//	@Column(name = "password")
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String password;

	@JsonIgnore
	private String verificationToken;

//	@Column(name = "cin")
	private String cin;

//	@Column(name = "datenaissance")
	private String datenaissance;

//	@Column(name = "numeromatriculation")
	private String numeromatriculation;
//	@Column(name = "numerotelephone")
	private String numerotelephone;
//	@Column(name = "numeropatente")
	private String numeropatente;

	private String userId;





	public Employee() {

	}

	public Employee(String firstName, String lastName, String emailId, String file, String password, String cin, String date, String numeromatriculation, String numerotelephone,  String numeropatente) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.file = file;
		this.cin = cin;
		this.datenaissance = date;
		this.numeromatriculation = numeromatriculation;
		this.numerotelephone = numerotelephone;
		this.numeropatente = numeropatente;

	}

	public static String randompassword(int len) {
		String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ@=^";
		Random rnd = new Random();

		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		}
		return sb.toString();
	}


}
