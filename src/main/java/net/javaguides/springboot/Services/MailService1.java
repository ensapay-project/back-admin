package net.javaguides.springboot.Services;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.javaguides.springboot.model.Employee;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.util.Properties;

@Service
@RequiredArgsConstructor
@Log4j2
public class MailService1 {

    private final HttpServletRequest request;

    private final  TemplateEngine templateEngine;

    @Value("${mail.from}")
    private String FROM;

    @Value("${spring.mail.host}")
    private String HOST;

    @Value("${spring.mail.port}")
    private String PORT;

    @Value("${spring.mail.properties.mail.smtp.auth}")
    private boolean SMTP_AUTH;

    @Value("${spring.mail.properties.mail.smtp.ssl.enable}")
    private boolean SMTP_SSL;

    @Value("${spring.mail.username}")
    private String USERNAME;

    @Value("${spring.mail.password}")
    private String PASSWORD;


    public void sendVerificationMail(Employee employee, String password) {
        try {
            MimeMessage message = getMimeMessage(
                    employee.getEmailId(),
                    "Verification d'email",
                    "Bienvenue "  + employee.getFirstName() + " sur ENSAPAY"
            );
            MimeMessageHelper messageHelper = new MimeMessageHelper(message);

            Context context = new Context();
            context.setVariable("receiver", employee);
            context.setVariable("password", password);
            String content = templateEngine.process("employee_info", context);
            messageHelper.setText(content, true);
            // Send message
            Transport.send(message);
            log.info("Email sent to " + employee.getFirstName() + " successfully.");
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
    }


    private MimeMessage getMimeMessage(String to, String subject, String text) throws MessagingException {
        // Create a default MimeMessage object.
        MimeMessage message = new MimeMessage(getMailSession());

        // Set From: header field of the header.
        message.setFrom(new InternetAddress(FROM));

        // Set To: header field of the header.
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

        // Set Subject: header field
        message.setSubject(subject);

        // Now set the actual message;
        message.setText(text);
        return message;
    }


    private Session getMailSession() {
        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.put("mail.smtp.host", HOST);
        properties.put("mail.smtp.port", PORT);
        properties.put("mail.smtp.ssl.enable", SMTP_SSL);
        properties.put("mail.smtp.auth", SMTP_AUTH);
        properties.put("mail.mime.charset", "UTF-8");

        // Get the Session objec
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USERNAME, PASSWORD);
            }

        });
        // Used to debug SMTP issues
        session.setDebug(true);
        return session;
    }

    private String getConfirmationURL(Employee user) {
        String rootURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        // generation du lien de confirmation et envoie par mail
        return rootURL + "/confirm?token=" + user.getVerificationToken();
    }

}
