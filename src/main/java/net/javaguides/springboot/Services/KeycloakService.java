package net.javaguides.springboot.Services;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.bytebuddy.utility.RandomString;
import net.javaguides.springboot.model.Employee;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.ws.rs.core.Response;
import java.util.Arrays;

@Component
@Log4j2
@AllArgsConstructor
public class KeycloakService {

    MailService1 mailService;

    public String createUser(Employee user) {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setEnabled(true);
        userRepresentation.setUsername(user.getEmailId());
        userRepresentation.setFirstName(user.getFirstName());
        userRepresentation.setLastName(user.getLastName());
        userRepresentation.setEmail(user.getEmailId());

        RealmResource realmResource = keycloakBuilder().realm("ensapay");
        UsersResource usersRessource = realmResource.users();
        Response response = usersRessource.create(userRepresentation);
        System.out.printf("Repsonse: %s %s%n", response.getStatus(), response.getStatusInfo());
        System.out.println(response.getLocation());
        RoleRepresentation agentRole = realmResource.roles()//
            .get("ROLE_agent").toRepresentation();
        String userId = CreatedResponseUtil.getCreatedId(response);
        UserResource userResource = usersRessource.get(userId);
        System.out.printf("User created with userId: %s%n", userId);
        userResource.roles().realmLevel() //
            .add(Arrays.asList(agentRole));

        return userId;
    }

    public void updateUser(Employee body) {
        RealmResource realmResource = keycloakBuilder().realm("ensapay");
        UserResource user = realmResource.users().get(body.getUserId());

        UserRepresentation userRepresentation = new UserRepresentation();
        if (body.getEmailId() != null) {
            userRepresentation.setEmail(body.getEmailId());
        }
        if (body.getFirstName() != null) {
            userRepresentation.setFirstName(body.getFirstName());
        }
        if (body.getLastName() != null) {
            userRepresentation.setLastName(body.getLastName());
        }

        user.update(userRepresentation);

    }

    public void deleteUser(String id) {
        RealmResource realmResource = keycloakBuilder().realm("ensapay");
        UsersResource usersRessource = realmResource.users();
        Response response = usersRessource.delete(id);
        System.out.printf("Repsonse: %s %s %n", response.getStatus(), response.getStatusInfo());
    }

    public void resetPassword(Employee employee, String randomPassword) {
        RealmResource realmResource = keycloakBuilder().realm("ensapay");
        UserResource user = realmResource.users().get(employee.getUserId());
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(randomPassword);
        credentialRepresentation.setTemporary(false);
        user.resetPassword(credentialRepresentation);
        log.info("Generated Password : {}", randomPassword);
        mailService.sendVerificationMail(employee, randomPassword);

//        mailService.sendPasswordRecoveryMail(employee.getAccount(), randomPassword);
    }

    protected Keycloak keycloakBuilder() {
        return KeycloakBuilder.builder()
            .serverUrl("https://ensapay-auth-server.herokuapp.com/auth") //
            .realm("ensapay") //
            .grantType(OAuth2Constants.PASSWORD) //
            .clientId("resource-server") //
            .username("admin") //
            .password("pass") //
            .build();
    }
}
