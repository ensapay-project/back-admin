//package net.javaguides.springboot.Services;
//
//
//
//
//import javax.annotation.processing.AbstractProcessor;
//import javax.mail.*;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeMessage;
//
//import net.javaguides.springboot.model.Employee;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.io.ClassPathResource;
//import org.springframework.core.io.FileSystemResource;
//import org.springframework.mail.MailException;
//import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.MimeMessageHelper;
//import org.springframework.stereotype.Service;
//import org.thymeleaf.context.Context;
//
//import java.util.Properties;
//
//
//@Service
//public class MailService {
//
//    /*
//     * The Spring Framework provides an easy abstraction for sending email by using
//     * the JavaMailSender interface, and Spring Boot provides auto-configuration for
//     * it as well as a starter module.
//     */
//    private JavaMailSender javaMailSender;
//
//    /**
//     *
//     * @param javaMailSender
//     */
//    @Autowired
//    public MailService(JavaMailSender javaMailSender) {
//        this.javaMailSender = javaMailSender;
//    }
//
//
//    /**
//     * This function is used to send mail without attachment.
//     * @param employee
//     * @throws MailException
//     */
//
//    public void  sendEmail(Employee employee) throws MailException {
//
//
//        SimpleMailMessage mail = new SimpleMailMessage();
//        mail.setTo(employee.getEmailId());
//        mail.setSubject("Password");
//        mail.setText("Voici votre Password");
//
//        javaMailSender.send(mail);
//    }
//
//    /**
//     * This fucntion is used to send mail that contains a attachment.
//     *
//     * @param employee
//     * @throws MailException
//     * @throws MessagingException
//     */
//    public void sendEmailWithAttachment(Employee employee) throws MailException, MessagingException {
//
//        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
//
//        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
//
//        helper.setTo(employee.getEmailId());
//        helper.setSubject("Testing Mail API with Attachment");
//        helper.setText("Please find the attached document below.");
//
//        ClassPathResource classPathResource = new ClassPathResource("Password");
//        helper.addAttachment(classPathResource.getFilename(), classPathResource);
//
//        javaMailSender.send(mimeMessage);
//    }
//  //  public void sendVerificationMail (Employee employee) {
//      //  try {
//           // MimeMessage message = getMimeMessage(
//              //      employee.getPassword(),
//               //     "Verification d'email",
//               //     "Bienvenue "  + employee.getFirstName() + " sur RH"
//           // );
//          //  MimeMessageHelper messageHelper = new MimeMessageHelper(message);
//
////            context.setVariable("receiver", employee);
//          //  String content = templateEngine.process("mails/Agent_info", context);
//         //   messageHelper.setText(content, true);
//            // Send message
//        //    Transport.send(message);
//      //  } catch (MessagingException ex) {
//        //    ex.printStackTrace();
//      //  }
//    //}
//
//    public void sendPasswordRecoveryMail (Employee user) {
//        String url = getConfirmationURL(user) + "&action=forgot_password";
//        try {
//            MimeMessage message = getMimeMessage(
//                    user.getEmailId(),
//                    "Récuperation du mot de passe",
//                    "Bienvenue "  + user.getLastName()
//            );
//            MimeMessageHelper messageHelper = new MimeMessageHelper(message);
//
//            Context context = new Context();
//            context.setVariable("url", url);
//            context.setVariable("receiver", user);
//            String content = templateEngine.process("mails/forgot_password", context);
//            messageHelper.setText(content, true);
//            // Send message
//            Transport.send(message);
//        } catch (MessagingException ex) {
//            ex.printStackTrace();
//        }
//    }
//    public void sendVerificationMail (Employee employee) {
//        String url = getConfirmationURL(employee) + "&action=confirm";
//        try {
//            MimeMessage message = getMimeMessage(
//                    employee.getPassword(),
//                    "Verification d'email",
//                    "Bienvenue "  + employee.getFirstName() + " sur RH"
//            );
//            MimeMessageHelper messageHelper = new MimeMessageHelper(message);
//
//            Context context = new Context();
//            context.setVariable("url", url);
//            context.setVariable("receiver", employee);
//            String content = templateEngine.process("mails/Agent password", context);
//            messageHelper.setText(content, true);
//            // Send message
//            Transport.send(message);
//        } catch (MessagingException ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    private MimeMessage getMimeMessage(String to, String subject, String text) throws MessagingException {
//        // Create a default MimeMessage object.
//        MimeMessage message = new MimeMessage(getMailSession());
//
//        // Set From: header field of the header.
//        message.setFrom(new InternetAddress(FROM));
//
//        // Set To: header field of the header.
//        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
//
//        // Set Subject: header field
//        message.setSubject(subject);
//
//        // Now set the actual message;
//        message.setText(text);
//        return message;
//    }
//
//
//    private Session getMailSession() {
//        // Get system properties
//        Properties properties = System.getProperties();
//
//        // Setup mail server
//        properties.put("mail.smtp.host", HOST);
//        properties.put("mail.smtp.port", PORT);
//        properties.put("mail.smtp.ssl.enable", SMTP_SSL);
//        properties.put("mail.smtp.auth", SMTP_AUTH);
//        properties.put("mail.mime.charset", "UTF-8");
//
//        // Get the Session objec
//        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
//
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(USERNAME, PASSWORD);
//            }
//
//        });
//        // Used to debug SMTP issues
//        session.setDebug(true);
//        return session;
//    }
//
//    private String getConfirmationURL(Employee user) {
//        String rootURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
//        // generation du lien de confirmation et envoie par mail
//        return rootURL + "/confirm?token=" + user.getVerificationToken();
//    }
//
//}
//
