package net.javaguides.springboot.controller;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import net.javaguides.springboot.Services.KeycloakService;
import net.javaguides.springboot.Services.MailService1;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Employee;
import net.javaguides.springboot.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.javaguides.springboot.model.Employee.randompassword;

//import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/admin/api/")
@RequiredArgsConstructor
@Api(description = "API CRUD des employées")
public class EmployeeController {

    private final EmployeeRepository employeeRepository;
    private final MailService1 mailService1;
    private final KeycloakService keycloakService;

    // get all employees
    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    // create employee rest api
    @PostMapping("/employees")
    public Employee createEmployee(@RequestParam("firstName") String firstName,
                                   @RequestParam("lastName") String lastName,
                                   @RequestParam("emailId") String emailId,
                                   @RequestParam("file") MultipartFile file,
                                   @RequestParam("cin") String cin,
                                   @RequestParam("datenaissance") String datenaissance,
                                   @RequestParam("numeromatriculation") String numeromatriculation,
                                   @RequestParam("numerotelephone") String numerotelephone,
                                   @RequestParam("numeropatente") String numeropatente) {

        if (employeeRepository.findByEmailId(emailId).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Client deja exist");
        }


        Employee employee = new Employee();
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setEmailId(emailId);
        employee.setCin(cin);
        employee.setDatenaissance(datenaissance);
        employee.setNumeromatriculation(numeromatriculation);
        employee.setNumerotelephone(numerotelephone);
        employee.setNumeropatente(numeropatente);

        try {
            System.out.printf("File name=%s, size=%s\n", file.getOriginalFilename(), file.getSize());
            //creating a new file in some local directory
            File fileToSave = new File("C:\\test\\" + file.getOriginalFilename());
            //copy file content from received file to new local file
            file.transferTo(fileToSave);
            employee.setFile(fileToSave.getAbsolutePath());
        } catch (IOException ioe) {
            //if something went bad, we need to inform client about it
        }
        employee.setUserId(keycloakService.createUser(employee));

        String randomPassword = randompassword(8);
        keycloakService.resetPassword(employee, randomPassword);
        return employeeRepository.save(employee);

    }

    // get employee by id rest api
    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable Long id) {
        Employee employee = employeeRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Employee not exist with id :" + id));
        return ResponseEntity.ok(employee);
    }


    @PostMapping("/reset-password")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void resetPassword(@RequestBody Employee user) {
        keycloakService.resetPassword(
            employeeRepository.findByEmailId(user.getEmailId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Email est introuvable")),
            randompassword(8)
        );
    }

    // update employee rest api

    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable Long id,
                                                   @RequestParam("firstName") String firstName,
                                                   @RequestParam("lastName") String lastName,
                                                   @RequestParam("emailId") String emailId,
                                                   @RequestParam("file") MultipartFile file,
                                                   @RequestParam("cin") String cin,
                                                   @RequestParam("datenaissance") String datenaissance,
                                                   @RequestParam("numeromatriculation") String numeromatriculation,
                                                   @RequestParam("numerotelephone") String numerotelephone,
                                                   @RequestParam("numeropatente") String numeropatente
    ) {


        Employee employee = employeeRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Employee not exist with id :" + id));

        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setEmailId(emailId);
        employee.setFirstName(cin);
        employee.setDatenaissance(datenaissance);
        employee.setNumeromatriculation(numeromatriculation);
        employee.setNumerotelephone(numerotelephone);
        employee.setNumeropatente(numeropatente);


        try {
            System.out.printf("File name=%s, size=%s\n", file.getOriginalFilename(), file.getSize());
            //creating a new file in some local directory
            File fileToSave = new File("C:\\test\\" + file.getOriginalFilename());
            //copy file content from received file to new local file
            file.transferTo(fileToSave);
            employee.setFile(fileToSave.getAbsolutePath());
        } catch (IOException ioe) {
            //if something went bad, we need to inform client about it
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }


        Employee updatedEmployee = employeeRepository.save(employee);
        keycloakService.updateUser(employee);

        return ResponseEntity.ok(updatedEmployee);
    }

    // delete employee rest api
    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteEmployee(@PathVariable Long id) {
        Employee employee = employeeRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Employee not exist with id :" + id));

        keycloakService.deleteUser(employee.getUserId());
        employeeRepository.delete(employee);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return ResponseEntity.ok(response);
    }


}
